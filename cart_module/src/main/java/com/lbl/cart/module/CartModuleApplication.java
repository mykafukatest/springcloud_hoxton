package com.lbl.cart.module;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class CartModuleApplication {

    public static void main(String[] args) {
        SpringApplication.run(CartModuleApplication.class, args);
    }

}
