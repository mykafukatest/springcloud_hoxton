package com.lbl.cart.module.controller;

import cn.hutool.core.collection.CollUtil;
import com.lbl.cart.module.api.service.CartService;
import com.lbl.cart.module.api.service.pojo.Cart;
import com.lbl.cart.module.api.service.pojo.CartItem;
import com.lbl.common.utils.ResultMap;
import com.lbl.goods.module.api.pojo.TradeGoods;
import com.lbl.goods.module.api.service.TradeGoodsService;
import com.lbl.ordermodule.api.pojo.TradeOrder;
import com.lbl.ordermodule.api.pojo.TradeOrderItem;
import com.lbl.ordermodule.api.service.TradeOrderService;
import com.lbl.ordermodule.api.vo.OrderVo;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Array;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author lbl
 * @version 1.0
 * @date 2020/7/27 14:50
 */
@RestController
@RequestMapping("cart")
public class CartController {

    @Autowired
    private CartService cartService;

    @Reference(version = "1.0.0",interfaceClass = TradeOrderService.class)
    private TradeOrderService tradeOrderService;

    @Reference(version = "1.0.0",interfaceClass = TradeGoodsService.class)
    private TradeGoodsService tradeGoodsService;

    @RequestMapping("/getCart")
    public Cart getCart(@RequestParam("token")String token){
        return cartService.getCart(token).get();
    }

    /**
     * 添加购物车条目
     * @param token
     * @param goodsId
     * @param count
     * @return
     */
    @RequestMapping("/addItem/{token}")
    public ResultMap addItem(@PathVariable("token")String token
            ,Long goodsId,Integer count){
        TradeGoods goods = tradeGoodsService.findById(goodsId);
        CartItem cartItem=new CartItem();
        cartItem.setGoods(goods);
        cartItem.setCount(count);
        cartItem.setGoodsId(goodsId);
        cartService.addCartItemToCart(token, Collections.singletonList(cartItem));
        return new ResultMap();
    }

    /**
     * 删除购物车条目
     * @param token
     * @param goodsId
     * @return
     */
    @RequestMapping("/delete/{goodsId}")
    public ResultMap deleteItem(@RequestParam("token")String token
            ,@PathVariable("goodsId") Long goodsId){
        cartService.romeveCartitem(token, Collections.singletonList(goodsId));
        return new ResultMap();
    }

    /**
     * 更新数量
     * @param token
     * @param goodsId
     * @param count
     * @return
     */
    @RequestMapping("/addCount/{goodsId}")
    public ResultMap updateCount(@RequestParam("token")String token
            ,@PathVariable("goodsId") Long goodsId,@RequestParam("count")Integer count){
        cartService.updateCartitemCount(token, goodsId,count);
        return new ResultMap();
    }

    /**
     * 生成预订单
     * @param token
     * @return
     */
    @RequestMapping("/generatePreOrder")
    public ResultMap generatePreOrder(@RequestParam("token")String token
            ,@RequestParam Long[] goodsIds){
        //获取购物车列表
        List<CartItem> cartItems = cartService.settleAccountsCartItems(token,Arrays.asList( goodsIds));

        if(CollUtil.isNotEmpty(cartItems)){
            OrderVo orderVo=new OrderVo();
            TradeOrder tradeOrder=new TradeOrder();
            tradeOrder.setOrderAmount(BigDecimal.ZERO);
            tradeOrder.setShippingFee(BigDecimal.ZERO);
            List<TradeOrderItem> tradeOrderItems=new ArrayList<>();
            Random random = new Random();
            for (CartItem cartItem : cartItems) {
                TradeOrderItem orderItem=new TradeOrderItem();

                orderItem.setGoodsId(cartItem.getGoodsId());
                orderItem.setGoodsNumber(cartItem.getCount());
                orderItem.setGoodsPrice(cartItem.getGoods().getGoodsPrice());
                orderItem.setGoodsAmount(cartItem.getSubTotla());
                orderItem.setGoodsName(cartItem.getGoods().getGoodsName());
                orderItem.setGoodsDesc(cartItem.getGoods().getGoodsDesc());
                orderItem.setShippingFee(BigDecimal.valueOf(random.nextInt(20)));
                //总运费
                tradeOrder.setShippingFee(orderItem.getShippingFee().add(tradeOrder.getShippingFee()));
                //总价格
                tradeOrder.setOrderAmount(tradeOrder.getOrderAmount().add(orderItem.getGoodsAmount()));
                tradeOrderItems.add(orderItem);
            }
            orderVo.setItemList(tradeOrderItems);
            orderVo.setTradeOrder(tradeOrder);

            //生成订单
            Long order = tradeOrderService.createPreOrder(orderVo);
            //订单生成成功清除购物车的数据
            cartService.romeveCartitem(token,Arrays.asList(goodsIds));
            return ResultMap.ok().put("orderNo",String.valueOf(order));
        }
        return ResultMap.error("请选择订单！");

    }
}
