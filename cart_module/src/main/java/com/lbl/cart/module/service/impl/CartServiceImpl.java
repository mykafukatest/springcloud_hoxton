package com.lbl.cart.module.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.json.JSONUtil;
import com.google.common.collect.Lists;
import com.lbl.cart.module.api.service.CartService;
import com.lbl.cart.module.api.service.pojo.Cart;
import com.lbl.cart.module.api.service.pojo.CartItem;
import com.lbl.goods.module.api.pojo.TradeGoods;
import com.lbl.goods.module.api.service.TradeGoodsService;
import com.lbl.ordermodule.api.service.TradeOrderService;
import com.lbl.paymentmodule.api.service.AlipayService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;

import java.math.BigDecimal;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

/**
 * @author lbl
 * @version 1.0
 * @date 2020/7/27 13:58
 */
@Slf4j
@Service(version = "1.0.0",
        interfaceClass = CartService.class,
        cluster = "failfast",loadbalance = "roundrobin")
public class CartServiceImpl implements CartService {

    @Autowired
    private RedisTemplate<Object,Object> redisTemplatel;

    @Reference(version = "1.0.0",interfaceClass = TradeGoodsService.class)
    private TradeGoodsService tradeGoodsService;

    private final String CART_PREFIX="CART_PRIFIX";

    private final Integer TIMEOUTCART=60;

    @Override
    public void addCartItemToCart(String token, List<CartItem> items) {
        Optional<Cart> oldOptionalCart = this.getCart(token);
        Cart oldCart = oldOptionalCart.get();
        Map<Long, List<CartItem>> map = items.stream().collect(Collectors.groupingBy(CartItem::getGoodsId));
        map.forEach((k,v)->{
            Map<Long, CartItem> itemMap = oldCart.getItems();
            int sum = v.stream().mapToInt(CartItem::getCount).sum();
            BigDecimal adSubtotal=BigDecimal.valueOf(sum).multiply(v.get(0).getGoods().getGoodsPrice());
            if(itemMap.containsKey(k)){
                CartItem item = itemMap.get(k);
                item.setCount(item.getCount()+sum);
                item.setSubTotla(item.getSubTotla().add(adSubtotal));
            }else{
                TradeGoods goods = v.get(0).getGoods();
                CartItem cartItem=new CartItem();
                cartItem.setGoods(goods);
                cartItem.setCount(sum);
                cartItem.setSubTotla(adSubtotal);
                cartItem.setGoodsId(k);
                oldCart.getItems().put(k,cartItem);
            }
            oldCart.setTotal(oldCart.getTotal().add(adSubtotal));
        });
        log.info("添加购物车数据：{}",JSONUtil.toJsonStr(oldCart));
        redisTemplatel.opsForValue().set(CART_PREFIX+token, JSONUtil.toJsonStr(oldCart),TIMEOUTCART, TimeUnit.MINUTES);
    }

    @Override
    public Optional<Cart> getCart(String token) {
        Object o = redisTemplatel.opsForValue().get(CART_PREFIX + token);
        log.info("根据{}获取购物车数据：{}",CART_PREFIX + token,String.valueOf(o));
        return Objects.nonNull(o)? Optional.of(JSONUtil.toBean(String.valueOf(o),Cart.class)):Optional.of(new Cart());
    }

    @Override
    public void cleanCart(String token) {
        log.info("清空购物车:{}",CART_PREFIX + token);
        redisTemplatel.delete(CART_PREFIX + token);
    }

    @Override
    public void romeveCartitem(String token, List<Long> goodId) {
        Optional<Cart> oldOptionalCart = this.getCart(token);
        Cart oldCart = oldOptionalCart.get();
        Map<Long, CartItem> items = oldCart.getItems();
        for (Long aLong : goodId) {
            CartItem cartItem = items.get(aLong);
            oldCart.setTotal(oldCart.getTotal().subtract(cartItem.getSubTotla()));
            items.remove(aLong);
        }
        log.info("删除购物车：{}",JSONUtil.toJsonStr(oldCart));
        redisTemplatel.opsForValue().set(CART_PREFIX+token, JSONUtil.toJsonStr(oldCart),TIMEOUTCART, TimeUnit.MINUTES);
    }

    @Override
    public void updateCartitemCount(String token, Long goodId, Integer count) {
        Optional<Cart> oldOptionalCart = this.getCart(token);
        Cart oldCart = oldOptionalCart.get();
        CartItem cartItem = oldCart.getItems().get(goodId);
        cartItem.setCount(count);
        log.info("删除购物车：{}",JSONUtil.toJsonStr(oldCart));
        redisTemplatel.opsForValue().set(CART_PREFIX+token, JSONUtil.toJsonStr(oldCart),TIMEOUTCART, TimeUnit.MINUTES);
    }

    @Override
    public List<CartItem> settleAccountsCartItems(String token, List<Long> goodIds) {
        List<CartItem> list = Lists.newArrayList();

        Optional<Cart> oldOptionalCart = this.getCart(token);
        Cart oldCart = oldOptionalCart.get();

        Map<Long, CartItem> items = oldCart.getItems();
        if(CollUtil.isNotEmpty(items)){
            for (Long goodId : goodIds) {
                list.add(items.get(goodId));
            }
        }
        log.info("结算购物车条目：{}",JSONUtil.toJsonStr(list));
        return list;
    }
}
