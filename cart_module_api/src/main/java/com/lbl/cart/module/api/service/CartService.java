package com.lbl.cart.module.api.service;

import com.lbl.cart.module.api.service.pojo.Cart;
import com.lbl.cart.module.api.service.pojo.CartItem;

import java.util.List;
import java.util.Optional;

/**
 * @author lbl
 * @version 1.0
 * @date 2020/7/27 13:48
 */
public interface CartService {

    /**
     * 添加购物车条目
     * @param token
     * @param items
     */
    void addCartItemToCart(String token, List<CartItem> items);

    /**
     * 获取购物车
     * @param token
     * @return
     */
    Optional<Cart> getCart(String token);

    /**
     * 清空购物车
     * @param token
     */
    void cleanCart(String token);

    /**
     * 删除购物车条目
     * @param token
     * @param goodId
     */
    void romeveCartitem(String token,List<Long> goodId);

    /**
     * 更改购物车某一商品的数量
     * @param token
     */
    void updateCartitemCount(String token,Long goodId,Integer count);

    /**
     * 获取要结算的购物车条目
     * @param goodsId
     * @return
     */
    List<CartItem> settleAccountsCartItems(String token,List<Long> goodsId);
}
