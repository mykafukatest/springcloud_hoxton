package com.lbl.cart.module.api.service.pojo;

import lombok.Data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.*;

/**
 * @author lbl
 * @version 1.0
 * @date 2020/7/27 13:48
 */
@Data
public class Cart implements Serializable {


    private BigDecimal total=BigDecimal.ZERO;

    private Map<Long,CartItem> items=new LinkedHashMap<>();

}
