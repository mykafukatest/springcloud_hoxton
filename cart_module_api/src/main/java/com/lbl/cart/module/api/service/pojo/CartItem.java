package com.lbl.cart.module.api.service.pojo;

import com.lbl.goods.module.api.pojo.TradeGoods;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;

/**
 * @author lbl
 * @version 1.0
 * @date 2020/7/27 13:49
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CartItem implements Serializable {
    private Long goodsId;

    private TradeGoods goods;
    private int count;
    private BigDecimal subTotla;

}
