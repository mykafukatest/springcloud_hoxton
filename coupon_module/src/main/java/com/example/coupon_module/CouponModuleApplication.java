package com.example.coupon_module;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CouponModuleApplication {

    public static void main(String[] args) {
        SpringApplication.run(CouponModuleApplication.class, args);
    }

}
