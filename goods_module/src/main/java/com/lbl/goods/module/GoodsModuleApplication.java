package com.lbl.goods.module;

import com.lbl.goods.module.stream.MessageSources;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.stream.annotation.EnableBinding;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@EnableDiscoveryClient
@MapperScan("com.lbl.goods.module.mapper")
@EnableBinding(MessageSources.class)
public class GoodsModuleApplication {

    public static void main(String[] args) {
        SpringApplication.run(GoodsModuleApplication.class, args);
    }

}
