package com.lbl.goods.module.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.lbl.common.utils.PageResult;
import com.lbl.goods.module.api.pojo.TradeGoods;
import com.lbl.goods.module.api.service.TradeGoodsService;
import com.lbl.goods.module.controller.falllback.GoodsRestControllerFallback;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lbl
 * @version 1.0
 * @date 2020/7/27 9:36
 */
@Slf4j
@RestController
@RequestMapping("goods")
public class GoodsRestController {

    @Autowired
    private TradeGoodsService tradeGoodsService;

    @Value("${server.port}")
    private String port;

    @SentinelResource(value = "queryTradeGoodList",
            fallbackClass = GoodsRestControllerFallback.class,
            fallback = "queryTradeGoodListFallback")
    @RequestMapping("/queryTradeGoodList/{pageNo}/{pageSize}")
    public PageResult<TradeGoods> queryTradeGoodList(
            @PathVariable("pageNo")Integer pageNo,
            @PathVariable("pageSize")Integer pageSize){
        log.info("queryTradeGoodList [{}]",port);
        return tradeGoodsService.queryTradeGoodList(pageNo,pageSize);
    }

    @SentinelResource(value = "findById",
            fallbackClass = GoodsRestControllerFallback.class,
            fallback = "findByIdFallback")
    @RequestMapping("/findById/{goodsId}")
    public TradeGoods findById(@PathVariable("goodsId")Long goodsId) throws InterruptedException {
//        Thread.sleep(goodsId*1000);
        log.info("queryTradeGoodList [{}]",port);
        return tradeGoodsService.findById(goodsId);
    }


}
