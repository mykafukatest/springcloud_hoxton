package com.lbl.goods.module.controller.falllback;

import com.lbl.common.utils.PageResult;
import com.lbl.goods.module.api.pojo.TradeGoods;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.Arrays;
import java.util.Collections;

/**
 * @author lbl
 * @version 1.0
 * @date 2020/8/5 10:21
 */
@Slf4j
public class GoodsRestControllerFallback {
    public static PageResult<TradeGoods> queryTradeGoodListFallback (Integer pageNo,Integer pageSize, Throwable e ) {
        log.error ("异常降级处理 ",e);
        //可以处理各种类型的异常，自定义异常
        if (e instanceof RuntimeException) {
            System.out.println ("异常类型");
        }
        return new PageResult<>(0, Collections.emptyList());
    }
    public static TradeGoods findByIdFallback (Long goodsId, Throwable e ) {
        log.error ("异常降级处理 ",e);
        //可以处理各种类型的异常，自定义异常
        if (e instanceof RuntimeException) {
            System.out.println ("异常类型");
        }
        return new TradeGoods();
    }
}
