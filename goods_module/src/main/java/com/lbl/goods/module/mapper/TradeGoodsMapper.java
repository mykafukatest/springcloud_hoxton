package com.lbl.goods.module.mapper;

import com.lbl.goods.module.api.pojo.TradeGoods;
import tk.mybatis.mapper.common.Mapper;

public interface TradeGoodsMapper extends Mapper<TradeGoods> {
}