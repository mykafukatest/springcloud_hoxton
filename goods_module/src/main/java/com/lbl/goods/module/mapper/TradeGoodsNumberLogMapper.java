package com.lbl.goods.module.mapper;

import com.lbl.goods.module.api.pojo.TradeGoodsNumberLog;
import tk.mybatis.mapper.common.Mapper;

public interface TradeGoodsNumberLogMapper extends Mapper<TradeGoodsNumberLog> {
}