package com.lbl.goods.module.mapper;

import com.lbl.goods.module.api.pojo.TradeMqConsumerLog;
import tk.mybatis.mapper.common.Mapper;

public interface TradeMqConsumerLogMapper extends Mapper<TradeMqConsumerLog> {
}