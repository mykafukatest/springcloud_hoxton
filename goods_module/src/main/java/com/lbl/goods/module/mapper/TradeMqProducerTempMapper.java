package com.lbl.goods.module.mapper;

import com.lbl.goods.module.api.pojo.TradeMqProducerTemp;
import tk.mybatis.mapper.common.Mapper;

public interface TradeMqProducerTempMapper extends Mapper<TradeMqProducerTemp> {
}