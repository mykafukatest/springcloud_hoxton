package com.lbl.goods.module.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lbl.common.utils.PageResult;
import com.lbl.goods.module.api.pojo.TradeGoods;
import com.lbl.goods.module.mapper.TradeGoodsMapper;
import com.lbl.goods.module.api.service.TradeGoodsService;
import org.apache.dubbo.config.annotation.Service;
import tk.mybatis.mapper.entity.Example;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author  lbl
 * @date  2020/7/24 14:31
 */
@Service(version = "1.0.0",
        interfaceClass = TradeGoodsService.class,
        cluster = "failfast",loadbalance = "roundrobin")
public class TradeGoodsServiceImpl implements TradeGoodsService {

    @Resource
    private TradeGoodsMapper tradeGoodsMapper;

    @Override
    public PageResult<TradeGoods> queryTradeGoodList(Integer pageNo, Integer pageSize) {
        PageHelper.startPage(pageNo,pageSize);
        PageInfo<TradeGoods> info=new PageInfo<>(tradeGoodsMapper.selectAll());
        return new PageResult<TradeGoods>(info.getTotal(),info.getList());
    }

    @Override
    public Map<Long,TradeGoods> findByIds(List<Long> goodsIds) {
        Example example=new Example(TradeGoods.class);
        example.createCriteria().andIn("goodsId",goodsIds);
        List<TradeGoods> tradeGoods = tradeGoodsMapper.selectByExample(example);
        return tradeGoods.stream().collect(Collectors.toMap(TradeGoods::getGoodsId,item->item));
    }

    @Override
    public TradeGoods findById(Long goodsId) {
        return tradeGoodsMapper.selectByPrimaryKey(goodsId);
    }
}
