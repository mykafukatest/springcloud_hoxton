package com.lbl.goods.module.service.impl;

import com.lbl.goods.module.mapper.TradeMqProducerTempMapper;
import com.lbl.goods.module.service.TradeMqProducerTempService;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
/**
 * @author  lbl
 * @date  2020/7/24 14:39
 */
@Service
public class TradeMqProducerTempServiceImpl implements TradeMqProducerTempService {

    @Resource
    private TradeMqProducerTempMapper tradeMqProducerTempMapper;

}
