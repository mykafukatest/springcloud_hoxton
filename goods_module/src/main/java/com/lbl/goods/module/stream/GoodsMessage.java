package com.lbl.goods.module.stream;

import org.springframework.messaging.Message;

/**
 * @author lbl
 * @version 1.0
 * @date 2020/8/3 10:19
 */
public interface GoodsMessage {

    /**
     * 跟新库存数量
     */
    void goodsMessage(Message<Long> message);
}
