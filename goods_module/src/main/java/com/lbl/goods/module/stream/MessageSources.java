package com.lbl.goods.module.stream;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

/**
 * @author lbl
 * @version 1.0
 * @date 2020/7/20 19:44
 */
public interface MessageSources {
    String GOODS_PAY_INPUT="goods-pay-input";

    /**
     *
     * @return
     */
    @Input(GOODS_PAY_INPUT)
    MessageChannel goodsPayInput();

}
