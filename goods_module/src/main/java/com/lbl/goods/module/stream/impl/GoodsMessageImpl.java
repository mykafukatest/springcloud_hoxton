package com.lbl.goods.module.stream.impl;

import com.lbl.goods.module.api.pojo.TradeGoods;
import com.lbl.goods.module.mapper.TradeGoodsMapper;
import com.lbl.goods.module.stream.GoodsMessage;
import com.lbl.goods.module.stream.MessageSources;
import com.lbl.ordermodule.api.pojo.TradeOrder;
import com.lbl.ordermodule.api.pojo.TradeOrderItem;
import com.lbl.ordermodule.api.service.TradeOrderService;
import com.lbl.ordermodule.api.vo.OrderVo;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @author lbl
 * @version 1.0
 * @date 2020/8/3 10:19
 */

@Slf4j
@Service
public class GoodsMessageImpl implements GoodsMessage {

    @Reference(version = "1.0.0",interfaceClass = TradeOrderService.class,check = false)
    private TradeOrderService tradeOrderService;

    @Autowired
    private TradeGoodsMapper tradeGoodsMapper;

    @Override
    @StreamListener(MessageSources.GOODS_PAY_INPUT)
    public void goodsMessage(Message<Long> message) {
        log.info("跟新库存:{}",message.getPayload());
        OrderVo orderVo = tradeOrderService.findByOrderId(message.getPayload());
        Map<Long, Integer> integerMap = orderVo.getItemList().stream().collect(Collectors.toMap(TradeOrderItem::getGoodsId, TradeOrderItem::getGoodsNumber));
        Example example=new Example(TradeGoods.class);
        example.createCriteria().andIn("goodsId",integerMap.keySet());
        List<TradeGoods> goods = tradeGoodsMapper.selectByExample(example);
        for (TradeGoods good : goods) {
            good.setGoodsNumber(good.getGoodsNumber()-integerMap.get(good.getGoodsId()));
            tradeGoodsMapper.updateByPrimaryKeySelective(good);
        }
    }
}
