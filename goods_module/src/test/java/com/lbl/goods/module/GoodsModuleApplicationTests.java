package com.lbl.goods.module;

import com.lbl.goods.module.mapper.TradeGoodsMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class GoodsModuleApplicationTests {

    @Autowired
    private TradeGoodsMapper tradeGoodsMapper;

    @Test
    void contextLoads() {
        System.out.println(tradeGoodsMapper.selectAll());
    }

}
