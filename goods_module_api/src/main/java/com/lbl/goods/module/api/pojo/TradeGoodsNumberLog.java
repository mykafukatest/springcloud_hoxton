package com.lbl.goods.module.api.pojo;

import java.util.Date;
import javax.persistence.*;
import lombok.Data;

@Data
@Table(name = "trade_goods_number_log")
public class TradeGoodsNumberLog {
    /**
     * 商品ID
     */
    @Id
    @Column(name = "goods_id")
    private Long goodsId;

    /**
     * 订单ID
     */
    @Id
    @Column(name = "order_id")
    private Long orderId;

    /**
     * 库存数量
     */
    @Column(name = "goods_number")
    private Integer goodsNumber;

    @Column(name = "log_time")
    private Date logTime;
}