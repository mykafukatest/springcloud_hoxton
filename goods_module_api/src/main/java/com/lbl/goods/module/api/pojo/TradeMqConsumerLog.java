package com.lbl.goods.module.api.pojo;

import java.util.Date;
import javax.persistence.*;
import lombok.Data;

@Data
@Table(name = "trade_mq_consumer_log")
public class TradeMqConsumerLog {
    @Id
    @Column(name = "group_name")
    private String groupName;

    @Id
    @Column(name = "msg_tag")
    private String msgTag;

    @Id
    @Column(name = "msg_key")
    private String msgKey;

    @Column(name = "msg_id")
    private String msgId;

    @Column(name = "msg_body")
    private String msgBody;

    /**
     * 0:正在处理;1:处理成功;2:处理失败
     */
    @Column(name = "consumer_status")
    private Integer consumerStatus;

    @Column(name = "consumer_times")
    private Integer consumerTimes;

    @Column(name = "consumer_timestamp")
    private Date consumerTimestamp;

    @Column(name = "remark")
    private String remark;
}