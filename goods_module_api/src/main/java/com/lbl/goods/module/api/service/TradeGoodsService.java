package com.lbl.goods.module.api.service;

import com.github.pagehelper.PageSerializable;
import com.lbl.common.utils.PageResult;
import com.lbl.goods.module.api.pojo.TradeGoods;

import java.util.List;
import java.util.Map;

/**
 * @author  lbl
 * @date  2020/7/24 14:31
 */
public interface TradeGoodsService{

    PageResult<TradeGoods> queryTradeGoodList(Integer pageNo, Integer pageSize);

    Map<Long,TradeGoods> findByIds(List<Long> goodsId);

    TradeGoods findById(Long goodsId);
}
