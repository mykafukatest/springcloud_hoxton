package com.lbl.ordermodule.controller;

import cn.hutool.json.JSONUtil;
import com.lbl.common.utils.ResultMap;
import com.lbl.ordermodule.api.constant.OrderConstant;
import com.lbl.ordermodule.api.service.TradeOrderService;
import com.lbl.ordermodule.api.vo.OrderVo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lbl
 * @version 1.0
 * @date 2020/7/29 14:50
 */
@Slf4j
@RestController
@RequestMapping("/order")
public class OrderController {
    @Autowired
    private TradeOrderService tradeOrderService;
    @Autowired
    private RedisTemplate<Object,Object> redisTemplatel;


    @RequestMapping("/orderInfo/{orderNo}")
    public ResultMap orderInfo(@PathVariable("orderNo")Long orderNo){
        OrderVo orderVo = tradeOrderService.findByOrderId(orderNo);
        log.info("查询订单[{}]详情：{}",orderNo, JSONUtil.toJsonStr(orderVo));
        return ResultMap.ok().put("data",orderVo);
    }

    @RequestMapping("/confirmOrder/{orderNo}")
    public ResultMap confirmOrder(@PathVariable("orderNo")Long orderNo){
        tradeOrderService.confirmOrder(orderNo);
        log.info("确认订单[{}]",orderNo);
        return ResultMap.ok();
    }

    @RequestMapping("/getOrderTtl/{orderNo}")
    public ResultMap getOrderTtl(@PathVariable("orderNo")Long orderNo){
        Long decrement = redisTemplatel.opsForValue().getOperations().getExpire(OrderConstant.ORDER_REDIS_TIME_OUT_PREFIX + orderNo);
        log.info("ttl[{}]",decrement);
        return ResultMap.ok().put("ttl",decrement);
    }

}
