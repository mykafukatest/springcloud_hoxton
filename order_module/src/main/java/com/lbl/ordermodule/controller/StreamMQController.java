package com.lbl.ordermodule.controller;

import com.lbl.ordermodule.api.service.TradeOrderService;
import com.lbl.ordermodule.stream.Message;
import com.lbl.ordermodule.stream.MessageSources;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Random;
import java.util.UUID;

/**
 * @author lbl
 * @version 1.0
 * @date 2020/7/20 20:27
 */
@RestController
@RequestMapping("streamMq")
public class StreamMQController {
}
