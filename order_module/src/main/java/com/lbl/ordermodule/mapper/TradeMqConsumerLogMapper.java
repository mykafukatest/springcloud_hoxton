package com.lbl.ordermodule.mapper;

import com.lbl.paymentmodule.api.pojo.TradeMqConsumerLog;
import tk.mybatis.mapper.common.Mapper;

public interface TradeMqConsumerLogMapper extends Mapper<TradeMqConsumerLog> {
}