package com.lbl.ordermodule.mapper;

import com.lbl.ordermodule.api.pojo.TradeOrderItem;
import tk.mybatis.mapper.common.Mapper;

public interface TradeOrderItemMapper extends Mapper<TradeOrderItem> {
}