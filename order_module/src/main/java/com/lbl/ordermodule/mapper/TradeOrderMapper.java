package com.lbl.ordermodule.mapper;

import com.lbl.ordermodule.api.pojo.TradeOrder;
import tk.mybatis.mapper.common.Mapper;

public interface TradeOrderMapper extends Mapper<TradeOrder> {
}