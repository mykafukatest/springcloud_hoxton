package com.lbl.ordermodule.service.impl;

import com.lbl.ordermodule.mapper.TradeMqConsumerLogMapper;
import com.lbl.paymentmodule.api.service.TradeMqConsumerLogService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author  lbl
 * @date  2020/7/17 15:57
 */
@Service
public class TradeMqConsumerLogServiceImpl implements TradeMqConsumerLogService {

    @Resource
    private TradeMqConsumerLogMapper tradeMqConsumerLogMapper;

}
