package com.lbl.ordermodule.service.impl;

import com.lbl.ordermodule.mapper.TradeMqProducerTempMapper;
import com.lbl.paymentmodule.api.service.TradeMqProducerTempService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @author  lbl
 * @date  2020/7/17 15:57
 */
@Service
public class TradeMqProducerTempServiceImpl implements TradeMqProducerTempService {

    @Resource
    private TradeMqProducerTempMapper tradeMqProducerTempMapper;

}
