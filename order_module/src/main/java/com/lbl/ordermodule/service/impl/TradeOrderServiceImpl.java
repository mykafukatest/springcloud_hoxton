package com.lbl.ordermodule.service.impl;

import com.lbl.common.utils.IdWorker;
import com.lbl.ordermodule.api.pojo.TradeOrder;
import com.lbl.ordermodule.api.pojo.TradeOrderItem;
import com.lbl.ordermodule.api.service.TradeOrderService;
import com.lbl.ordermodule.api.vo.OrderVo;
import com.lbl.ordermodule.mapper.TradeOrderItemMapper;
import com.lbl.ordermodule.mapper.TradeOrderMapper;
import com.lbl.ordermodule.stream.MessageService;
import com.lbl.ordermodule.stream.MessageSources;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.transaction.annotation.Transactional;
import tk.mybatis.mapper.entity.Example;

import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * @author  lbl
 * @date  2020/7/17 16:04
 */
@Service(version = "1.0.0",interfaceClass = TradeOrderService.class
        ,cluster = "failfast",loadbalance = "roundrobin")
public class TradeOrderServiceImpl implements TradeOrderService {

    @Autowired
    private TradeOrderMapper tradeOrderMapper;

    @Autowired
    private TradeOrderItemMapper tradeOrderItemMapper;

    @Autowired
    private IdWorker idWorker;

    @Autowired
    private MessageService messageService;

    @Override
    public List<TradeOrder> selectOrderList() {
        return tradeOrderMapper.selectAll();
    }

    @Override
    @Transactional
    public Long createPreOrder(OrderVo orderVo) {
        TradeOrder tradeOrder = orderVo.getTradeOrder();
        tradeOrder.setUserId(1433L);
        tradeOrder.setAddTime(new Date());
        tradeOrder.setOrderId(idWorker.nextId());
        tradeOrder.setOrderStatus(0);
        tradeOrder.setPayStatus(0);
        tradeOrder.setShippingStatus(0);
        tradeOrder.setConsignee("刘备");
        tradeOrder.setAddress("荆州");
        tradeOrderMapper.insert(tradeOrder);
        for (TradeOrderItem tradeOrderItem : orderVo.getItemList()) {
            tradeOrderItem.setOrderId(tradeOrder.getOrderId());
            tradeOrderItemMapper.insert(tradeOrderItem);
        }
        return tradeOrder.getOrderId();
    }

    @Override
    public OrderVo findByOrderId(Long orderId) {
        OrderVo orderVo=new OrderVo();
        TradeOrder tradeOrder = tradeOrderMapper.selectByPrimaryKey(orderId);
        Example example=new Example(TradeOrderItem.class);
        example.createCriteria().andEqualTo("orderId",orderId);
        List<TradeOrderItem> items = tradeOrderItemMapper.selectByExample(example);
        orderVo.setTradeOrder(tradeOrder);
        orderVo.setItemList(items);
        return orderVo;
    }

    @Override
    public TradeOrder getOrderId(Long orderId) {
        return  tradeOrderMapper.selectByPrimaryKey(orderId);
    }

    @Override
    public int updateOrderStatus(Long orderId, Integer status) {
        TradeOrder tradeOrder=new TradeOrder();
        tradeOrder.setOrderStatus(status);
        tradeOrder.setOrderId(orderId);
        return tradeOrderMapper.updateByPrimaryKeySelective(tradeOrder);
    }

    @Override
    public int updateOrderSelective(Long orderId, TradeOrder order) {
        order.setOrderId(orderId);
        return tradeOrderMapper.updateByPrimaryKeySelective(order);
    }

    @Override
    public void confirmOrder(Long orderId) {
        TradeOrder tradeOrder = tradeOrderMapper.selectByPrimaryKey(orderId);
        if(Objects.equals(tradeOrder.getOrderStatus(),0)){
            messageService.sendCancelOrder(orderId);
        }else {

            throw new RuntimeException("订单状态已发生改变稍后再试~！");
        }
    }
}
