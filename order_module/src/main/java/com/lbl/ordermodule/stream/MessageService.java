package com.lbl.ordermodule.stream;


import org.springframework.messaging.Message;

/**
 * @author lbl
 * @version 1.0
 * @date 2020/7/30 10:11
 */
public interface MessageService {
    /**
     * 发送订单确认消息
     * @param orderId
     */
    void sendCancelOrder(Long orderId);

    /**
     * 消费订单取消消息
     * @param message
     */
    void listenerCancelOrder(Message<Long> message);

    /**
     * 订单支付成功更新状态消息
     * @param message
     */
    void orderPaymentIsSuccessful(Message<Long> message);
}
