package com.lbl.ordermodule.stream;

import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

/**
 * @author lbl
 * @version 1.0
 * @date 2020/7/20 19:44
 */
public interface MessageSources {
    String RDER_CANCEL_OUTPUT="order-cancel-output";
    String RDER_CANCEL_INPUT="order-cancel-input";
    String RDER_PAY_INPUT="order-pay-input";

    /**
     * 订单取消发送通道
     * @return
     */
    @Output(RDER_CANCEL_OUTPUT)
    MessageChannel orderCancelOutput();

    /**
     * 订单取消接收通道
     * @return
     */
    @Input(RDER_CANCEL_INPUT)
    MessageChannel orderCancelInput();

    /**
     * 订单更新状态接收通道
     * @return
     */
    @Input(RDER_PAY_INPUT)
    MessageChannel rderPayInput();
}
