package com.lbl.ordermodule.stream.impl;

import com.lbl.ordermodule.api.constant.OrderConstant;
import com.lbl.ordermodule.api.pojo.TradeOrder;
import com.lbl.ordermodule.api.service.TradeOrderService;
import com.lbl.ordermodule.mapper.TradeOrderMapper;
import com.lbl.ordermodule.stream.MessageService;
import com.lbl.ordermodule.stream.MessageSources;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.core.RocketMQTemplate;
import org.apache.rocketmq.spring.support.RocketMQHeaders;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.integration.support.MessageBuilder;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageChannel;
import org.springframework.stereotype.Service;
import sun.rmi.runtime.Log;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * @author lbl
 * @version 1.0
 * @date 2020/7/20 19:48
 */
@Slf4j
@Service
public class MessageServiceImpl implements MessageService {
    private String messageDelayLevel = "1s 5s 10s 30s 1m 2m 3m 4m 5m 6m 7m 8m 9m 10m 20m 30m 1h 2h";
    @Autowired
    private MessageSources sources;
    @Autowired
    private TradeOrderService tradeOrderService;
    @Autowired
    private TradeOrderMapper tradeOrderMapper;

    @Autowired
    private RocketMQTemplate rocketMQTemplate;

    @Autowired
    private RedisTemplate<Object,Object> redisTemplatel;
    @Override
    public void sendCancelOrder(Long orderId) {
        TradeOrder tradeOrder = tradeOrderMapper.selectByPrimaryKey(orderId);
        if(Objects.equals(tradeOrder.getOrderStatus(), OrderConstant.ORDER_STATUS_UNCONFIRMED)){
            tradeOrderService.updateOrderStatus(orderId,OrderConstant.ORDER_STATUS_CONFIRMED);
            //设置过期时间
            redisTemplatel.opsForValue().set(OrderConstant.ORDER_REDIS_TIME_OUT_PREFIX+ orderId,1,3, TimeUnit.MINUTES);

            rocketMQTemplate.syncSend("order-topic",MessageBuilder.withPayload(orderId)
                    .setHeader("orderId",orderId)
                    .build(),2000,7);
//            sources.orderCancelOutput().send(MessageBuilder.withPayload(orderId)
//                    .setHeader("orderId",orderId)
//                    .setHeader("DELAY",14)
//                    .build());
            log.info("发送延迟取消订单消息 [orderId={}]",orderId);
        }
    }

    @Override
    @StreamListener(MessageSources.RDER_CANCEL_INPUT)
    public void listenerCancelOrder(Message<Long> message) {
        log.info("订单超时取消订单:{}",message.getPayload());
        TradeOrder tradeOrder = tradeOrderMapper.selectByPrimaryKey(message.getPayload());
        if(tradeOrder.getOrderStatus()<OrderConstant.ORDER_STATUS_CANCEL&&tradeOrder.getPayStatus()==0){
            //取消订单
            tradeOrderService.updateOrderStatus(message.getPayload(), OrderConstant.ORDER_STATUS_CANCEL);
        }
    }

    @Override
    @StreamListener(MessageSources.RDER_PAY_INPUT)
    public void orderPaymentIsSuccessful(Message<Long> message) {
        log.info("订单支付成功更新状态消息:{}",message.getPayload());
        TradeOrder tradeOrder=new TradeOrder();
        tradeOrder.setOrderId(message.getPayload());
        tradeOrder.setPayStatus(OrderConstant.ORDER_PAY_PAID);
        tradeOrder.setOrderStatus(OrderConstant.ORDER_STATUS_COMPLETED);
        tradeOrderMapper.updateByPrimaryKeySelective(tradeOrder);
    }
}
