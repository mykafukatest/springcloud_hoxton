package com.lbl.ordermodule.stream.impl;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.stream.binder.PartitionKeyExtractorStrategy;
import org.springframework.messaging.Message;
import org.springframework.stereotype.Component;

/**
 * @author lbl
 * @version 1.0
 * @date 2020/7/23 16:46
 */
//@Component
@Slf4j
public class MyPartitionKeyStrategy implements PartitionKeyExtractorStrategy {
    /***
     * 偶数partition=1 奇数 partition=0
     * @param message
     * @return
     */
    @Override
    public Object extractKey(Message<?> message) {
        com.lbl.ordermodule.stream.Message payload =(com.lbl.ordermodule.stream.Message) message.getPayload();
        int i = payload.getId() % 2 == 0 ? 1 : 0;
        log.info("PartitionKeyExtractorStrategy partition={}",i);
        return i;
    }
}
