package com.lbl.ordermodule.stream.impl;

import com.lbl.ordermodule.api.constant.OrderConstant;
import com.lbl.ordermodule.api.pojo.TradeOrder;
import com.lbl.ordermodule.api.service.TradeOrderService;
import com.lbl.ordermodule.api.vo.OrderVo;
import com.lbl.ordermodule.mapper.TradeOrderMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import sun.rmi.runtime.Log;

import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * @author lbl
 * @version 1.0
 * @date 2020/7/30 10:21
 */
@Slf4j
//@RocketMQTransactionListener(txProducerGroup = "orderProducerCancelGroup", corePoolSize = 5,
//        maximumPoolSize = 10)
public class OrderTransactionListenerImpl implements RocketMQLocalTransactionListener {

    @Autowired
    private RedisTemplate<Object,Object> redisTemplatel;

    @Autowired
    private TradeOrderService tradeOrderService;

    @Autowired
    private TradeOrderMapper tradeOrderMapper;


    @Override
    public RocketMQLocalTransactionState executeLocalTransaction(Message message, Object o) {
        MessageHeaders headers = message.getHeaders();
        String orderId =headers.get("orderId", String.class);
        try {
            TradeOrder tradeOrder = tradeOrderMapper.selectByPrimaryKey(Long.valueOf(orderId));
            if(Objects.equals(tradeOrder.getOrderStatus(), OrderConstant.ORDER_STATUS_UNCONFIRMED)){
                tradeOrderService.updateOrderStatus(Long.valueOf(orderId),OrderConstant.ORDER_STATUS_CONFIRMED);
                //设置过期时间
                redisTemplatel.opsForValue().set(OrderConstant.ORDER_REDIS_TIME_OUT_PREFIX+ orderId,1,3, TimeUnit.MINUTES);
            }
            return RocketMQLocalTransactionState.COMMIT;
        } catch (Exception e) {
            e.printStackTrace();
            return RocketMQLocalTransactionState.ROLLBACK;
        }
    }

    @Override
    public RocketMQLocalTransactionState checkLocalTransaction(Message message) {
        log.info("rocket mq check检测 [{}]",message.getPayload());
        return RocketMQLocalTransactionState.ROLLBACK;
    }
}
