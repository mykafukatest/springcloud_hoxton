package com.lbl.ordermodule.api.constant;

/**
 * @author lbl
 * @version 1.0
 * @date 2020/7/29 17:57
 */
public interface OrderConstant {
    /**
     * 取消订单过期时间key前缀
     */
    String ORDER_REDIS_TIME_OUT_PREFIX="ORDER_REDIS_TIME_OUT_PREFIX";

    /**
     * 订单状态 0未确认 1已确认 2已取消 3无效 4退款 5已完成
     */
    Integer ORDER_STATUS_UNCONFIRMED=0;
    Integer ORDER_STATUS_CONFIRMED=1;
    Integer ORDER_STATUS_CANCEL=2;
    Integer ORDER_STATUS_FAIL=3;
    Integer ORDER_STATUS_REIMBURSE=4;
    Integer ORDER_STATUS_COMPLETED=5;
    /**
     * 支付状态 0未支付 1支付中 2已支付
     */
    Integer ORDER_PAY_UNPAID=0;
    Integer ORDER_PAY_PAYMENTS=1;
    Integer ORDER_PAY_PAID=2;
}
