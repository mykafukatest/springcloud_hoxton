package com.lbl.ordermodule.api.pojo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import javax.persistence.*;
import lombok.Data;

@Data
@Table(name = "trade_order")
public class TradeOrder implements Serializable {
    /**
     * 订单ID
     */
    @Id
    @Column(name = "order_id")
    private Long orderId;

    /**
     * 用户ID
     */
    @Column(name = "user_id")
    private Long userId;

    /**
     * 订单状态 0未确认 1已确认 2已取消 3无效 4退款
     */
    @Column(name = "order_status")
    private Integer orderStatus;

    /**
     * 支付状态 0未支付 1支付中 2已支付
     */
    @Column(name = "pay_status")
    private Integer payStatus;

    /**
     * 发货状态 0未发货 1已发货 2已收货
     */
    @Column(name = "shipping_status")
    private Integer shippingStatus;

    /**
     * 收货地址
     */
    @Column(name = "address")
    private String address;

    /**
     * 收货人
     */
    @Column(name = "consignee")
    private String consignee;

    /**
     * 运费
     */
    @Column(name = "shipping_fee")
    private BigDecimal shippingFee;

    /**
     * 订单价格
     */
    @Column(name = "order_amount")
    private BigDecimal orderAmount;

    /**
     * 已付金额
     */
    @Column(name = "money_paid")
    private BigDecimal moneyPaid;

    /**
     * 支付金额
     */
    @Column(name = "pay_amount")
    private BigDecimal payAmount;

    /**
     * 创建时间
     */
    @Column(name = "add_time")
    private Date addTime;

    /**
     * 订单确认时间
     */
    @Column(name = "confirm_time")
    private Date confirmTime;

    /**
     * 支付时间
     */
    @Column(name = "pay_time")
    private Date payTime;
}