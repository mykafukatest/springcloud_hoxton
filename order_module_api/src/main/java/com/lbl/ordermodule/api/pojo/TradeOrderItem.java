package com.lbl.ordermodule.api.pojo;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;
import lombok.Data;

@Data
@Table(name = "trade_order_item")
public class TradeOrderItem implements Serializable {
    /**
     * id
     */
    @Id
    @Column(name = "order_item_id")
    @GeneratedValue(generator = "JDBC")
    private Integer orderItemId;
    /**
     * 订单ID
     */
    @Column(name = "order_id")
    private Long orderId;

    /**
     * 商品ID
     */
    @Column(name = "goods_id")
    private Long goodsId;

    /**
     * 商品数量
     */
    @Column(name = "goods_number")
    private Integer goodsNumber;

    /**
     * 商品价格
     */
    @Column(name = "goods_price")
    private BigDecimal goodsPrice;

    /**
     * 商品总价
     */
    @Column(name = "goods_amount")
    private BigDecimal goodsAmount;

    /**
     * 商品名称
     */
    @Column(name = "goods_name")
    private String goodsName;

    /**
     * 商品描述
     */
    @Column(name = "goods_desc")
    private String goodsDesc;

    /**
     * 运费
     */
    @Column(name = "shipping_fee")
    private BigDecimal shippingFee;

    /**
     * 订单价格
     */
    @Column(name = "order_amount")
    private BigDecimal orderAmount;

    /**
     * 优惠券ID
     */
    @Column(name = "coupon_id")
    private Long couponId;

    /**
     * 优惠券
     */
    @Column(name = "coupon_paid")
    private BigDecimal couponPaid;
}