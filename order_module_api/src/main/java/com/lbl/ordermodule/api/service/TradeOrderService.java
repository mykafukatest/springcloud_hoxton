package com.lbl.ordermodule.api.service;

import com.lbl.ordermodule.api.pojo.TradeOrder;
import com.lbl.ordermodule.api.vo.OrderVo;
import com.sun.org.apache.xpath.internal.operations.Or;

import java.util.List;

/**
 * @author  lbl
 * @date  2020/7/17 16:04
 */
public interface TradeOrderService{
    /**
     * 查询所有订单
     * @return
     */
    List<TradeOrder> selectOrderList();

    /**
     * 创建预处理订单
     * @param orderVo
     * @return
     */
    Long createPreOrder(OrderVo orderVo);

    /**
     * 根据订单No获取订单 (订单条目也会查询出来)
     * @param orderId
     * @return
     */
    OrderVo findByOrderId(Long orderId);


    /**
     * 根据订单No获取订单 (订单条目也会查询出来)
     * @param orderId
     * @return
     */
    TradeOrder getOrderId(Long orderId);

    /**
     * 修改订单状态
     * @param orderId
     */
    int updateOrderStatus(Long orderId,Integer status);

    /**
     * 选择性更新
     * @param orderId
     * @param order
     * @return
     */
    int updateOrderSelective(Long orderId,TradeOrder order);


    /**
     * 确认订单
     * @param orderId
     */
    void confirmOrder(Long orderId);



}
