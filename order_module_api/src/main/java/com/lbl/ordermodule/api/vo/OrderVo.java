package com.lbl.ordermodule.api.vo;

import com.lbl.ordermodule.api.pojo.TradeOrder;
import com.lbl.ordermodule.api.pojo.TradeOrderItem;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author lbl
 * @version 1.0
 * @date 2020/7/28 17:37
 */
@Data
public class OrderVo implements Serializable {
    private TradeOrder tradeOrder;
    private List<TradeOrderItem> itemList;
}
