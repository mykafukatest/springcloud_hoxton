package com.lbl.paymentmodule.consume;

import com.lbl.paymentmodule.api.pojo.TradeMqConsumerLog;
import com.lbl.paymentmodule.api.pojo.TradeMqProducerTemp;
import com.lbl.paymentmodule.api.service.TradeMqConsumerLogService;
import com.lbl.paymentmodule.api.service.TradeMqProducerTempService;
import com.lbl.paymentmodule.mapper.TradeMqConsumerLogMapper;
import com.lbl.paymentmodule.mapper.TradeMqProducerTempMapper;
import com.lbl.paymentmodule.stream.OrderInPutSink;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.Input;
import org.springframework.cloud.stream.annotation.StreamListener;
import org.springframework.cloud.stream.messaging.Sink;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Objects;
import java.util.UUID;

/**
 * @author lbl
 * @version 1.0
 * @date 2020/7/20 21:43
 */
@Slf4j
@Component
public class Listener {
    @Autowired
    private TradeMqProducerTempMapper tradeMqProducerTempMapper;

    @Autowired
    private TradeMqConsumerLogMapper tradeMqConsumerLogMapper;

    @StreamListener(OrderInPutSink.PAY_ORDER_CALLBACK_INPUT)
    @Transactional(rollbackFor = Exception.class)
    public void listener(Message<String> message){
        MessageHeaders headers = message.getHeaders();
        log.info("支付成功删除发送者消息：{}",message);
        String id = Objects.requireNonNull(headers.get("id", UUID.class)).toString();
        TradeMqConsumerLog log=new TradeMqConsumerLog();
        log.setMsgId(id);
        log.setMsgBody(message.getPayload());
        log.setMsgKey("*");
        log.setMsgTag("*");
        log.setRemark("消费成功");
        tradeMqConsumerLogMapper.insert(log);
        tradeMqProducerTempMapper.deleteByPrimaryKey(id);
    }
}
