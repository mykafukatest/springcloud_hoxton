package com.lbl.paymentmodule.controller;

import com.alipay.api.response.AlipayTradePagePayResponse;
import com.lbl.common.utils.ResultMap;
import com.lbl.ordermodule.api.constant.OrderConstant;
import com.lbl.ordermodule.api.pojo.TradeOrder;
import com.lbl.ordermodule.api.service.TradeOrderService;
import com.lbl.ordermodule.api.vo.OrderVo;
import com.lbl.paymentmodule.api.service.AlipayService;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
@Slf4j
@RestController
@RequestMapping("/alipay")
public class AlipayController  {

    @Reference(version ="1.0.0",interfaceClass = AlipayService.class)
    private AlipayService alipayService;

    @Reference(version ="1.0.0",interfaceClass = TradeOrderService.class)
    private TradeOrderService tradeOrderService;

    /**
     * 创建订单
     */
    @RequestMapping("/createOrder")
    public ResultMap createOrder(@RequestParam String orderNo) {
        try {
            // 1、验证订单是否存在
            OrderVo orderVo = tradeOrderService.findByOrderId(Long.valueOf(orderNo));
            if(orderVo!=null){
                // 2、创建支付宝订单
                TradeOrder tradeOrder = new TradeOrder();
                tradeOrder.setPayStatus(OrderConstant.ORDER_PAY_PAYMENTS);
                tradeOrderService.updateOrderSelective(Long.valueOf(orderNo),tradeOrder);
                AlipayTradePagePayResponse orderStr = alipayService.createOrder(orderNo, orderVo.getTradeOrder().getOrderAmount().doubleValue(), orderVo.getTradeOrder().getAddress()+orderVo.getTradeOrder().getConsignee());
                return ResultMap.ok().put("data", orderStr.getBody());
            }
            return ResultMap.error("订单不存在！");
        } catch (Exception e) {
            log.error(e.getMessage());
            return ResultMap.error("订单生成失败");
        }
    }

    /**
     * 支付异步通知
     * 接收到异步通知并验签通过后，一定要检查通知内容，
     * 包括通知中的app_id、out_trade_no、total_amount是否与请求中的一致，并根据trade_status进行后续业务处理。
     * https://docs.open.alipay.com/194/103296
     */
    @RequestMapping("/notify")
    public String notify(HttpServletRequest request) {
        // 验证签名
        boolean flag = alipayService.rsaCheckV1(request);
        if(!flag){
            return "fail";
        }
        // 交易状态
        String tradeStatus = request.getParameter("trade_status");
        // 商户订单号
        String outTradeNo = request.getParameter("out_trade_no");
        // 支付宝订单号
        String tradeNo = request.getParameter("trade_no");
        //支付金额
        String totalAmount = request.getParameter("total_amount");
        //还可以从request中获取更多有用的参数，自己尝试
        boolean notify = alipayService.notify(tradeStatus, outTradeNo, tradeNo,totalAmount);
        return notify?"success":"fail";
    }

    @PostMapping("/refund")
    public ResultMap refund(@RequestParam String orderNo,
                            @RequestParam double amount,
                            @RequestParam(required = false) String refundReason) {
        return alipayService.refund(orderNo, amount, refundReason);
    }


}