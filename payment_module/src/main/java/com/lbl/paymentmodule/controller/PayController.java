package com.lbl.paymentmodule.controller;

import com.lbl.ordermodule.api.pojo.TradeOrder;
import com.lbl.ordermodule.api.service.TradeOrderService;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @author lbl
 * @version 1.0
 * @date 2020/7/17 17:18
 */
@RestController
@RequestMapping("pay")
public class PayController {

    @Reference(version = "1.0.0",interfaceClass = TradeOrderService.class)
    public TradeOrderService tradeOrderService;

    @RequestMapping("/findOrderList")
    public List<TradeOrder> findOrderList(){
        return tradeOrderService.selectOrderList();
    }
}
