package com.lbl.paymentmodule.mapper;

import com.lbl.paymentmodule.api.pojo.TradeMqProducerTemp;
import tk.mybatis.mapper.common.Mapper;

public interface TradeMqProducerTempMapper extends Mapper<TradeMqProducerTemp> {
}