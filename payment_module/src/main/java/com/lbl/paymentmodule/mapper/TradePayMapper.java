package com.lbl.paymentmodule.mapper;

import com.lbl.paymentmodule.api.pojo.TradePay;
import tk.mybatis.mapper.common.Mapper;

public interface TradePayMapper extends Mapper<TradePay> {
}