package com.lbl.paymentmodule.service.impl;

import com.alipay.api.AlipayApiException;
import com.alipay.api.AlipayClient;
import com.alipay.api.domain.AlipayTradePagePayModel;
import com.alipay.api.domain.AlipayTradeRefundModel;
import com.alipay.api.domain.OrderItem;
import com.alipay.api.internal.util.AlipaySignature;
import com.alipay.api.internal.util.StringUtils;
import com.alipay.api.request.AlipayTradePagePayRequest;
import com.alipay.api.request.AlipayTradeRefundRequest;
import com.alipay.api.response.AlipayTradePagePayResponse;
import com.alipay.api.response.AlipayTradeRefundResponse;
import com.lbl.common.utils.ResultMap;
import com.lbl.ordermodule.api.constant.OrderConstant;
import com.lbl.ordermodule.api.pojo.TradeOrder;
import com.lbl.ordermodule.api.pojo.TradeOrderItem;
import com.lbl.ordermodule.api.service.TradeOrderService;
import com.lbl.ordermodule.api.vo.OrderVo;
import com.lbl.paymentmodule.api.service.AlipayService;
import com.lbl.paymentmodule.config.AlipayConfig;
import com.lbl.paymentmodule.stream.OrderOutPutSources;
import lombok.extern.slf4j.Slf4j;
import org.apache.dubbo.config.annotation.Reference;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.integration.support.MessageBuilder;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

/**
 * Author:XCK
 * Date:2019/8/1
 * Description:
 */
@Slf4j
@Service(version = "1.0.0",
        interfaceClass = AlipayService.class,
        cluster = "failfast",loadbalance = "roundrobin")
public class AlipayServiceImpl implements AlipayService {

    @Autowired
    private AlipayConfig alipayConfig;

    @Autowired
    private AlipayClient alipayClient;

    @Reference(version = "1.0.0",interfaceClass = TradeOrderService.class)
    public TradeOrderService tradeOrderService;

    @Autowired
    private OrderOutPutSources orderOutPutSources;

    @Value("${pay.distributed.transaction}")
    private String mode;

    @Override
    public AlipayTradePagePayResponse createOrder(String orderNo, double amount, String subject) throws AlipayApiException {
        //SDK已经封装掉了公共参数，这里只需要传入业务参数。以下方法为sdk的model入参方式(model和biz_content同时存在的情况下取biz_content)。
        AlipayTradePagePayModel model = new AlipayTradePagePayModel();
        model.setSubject(subject);
        model.setOutTradeNo(orderNo);
        model.setTotalAmount(String.valueOf(amount));
        model.setProductCode("FAST_INSTANT_TRADE_PAY");
        model.setPassbackParams("公用回传参数，如果请求时传递了该参数，则返回给商户时会回传该参数");
        model.setQrPayMode("2");
        //实例化具体API对应的request类,类名称和接口名称对应,当前调用接口名称：alipay.trade.app.pay
        AlipayTradePagePayRequest ali_request = new AlipayTradePagePayRequest();
        ali_request.setBizModel(model);
        // 回调地址
        ali_request.setNotifyUrl(alipayConfig.getNotifyUrl());
        //支付成功跳转页面
        ali_request.setReturnUrl(alipayConfig.getReturnUrl());
        AlipayTradePagePayResponse payResponse = alipayClient.pageExecute(ali_request);
        //就是orderString 可以直接给客户端请求，无需再做处理。
        return payResponse;
    }

    @Override
    public boolean notify(String tradeStatus, String orderNo, String tradeNo,String totalAmount) {
        if ("TRADE_FINISHED".equals(tradeStatus)
                || "TRADE_SUCCESS".equals(tradeStatus)) {
             //支付成功，根据业务逻辑修改相应数据的状态
            //rocketmq模式
            try {
                if(Objects.equals(mode,"0")){
                    //发送消息
                   log.info("支付宝回调(rocketmq)notify orderNo=[{}]",orderNo);
                   return orderOutPutSources.payOrderCallbackOutPut().send(MessageBuilder.withPayload(orderNo)
                            .setHeader("orderId", orderNo)
                            .setHeader("payId", tradeNo)
                            .setHeader("totalAmount", totalAmount)
                            .build());
                }else if(Objects.equals(mode,"1")){

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    @Override
    public boolean rsaCheckV1(HttpServletRequest request){
        try {
            Map<String, String> params = new HashMap<>();
            Map<String, String[]> requestParams = request.getParameterMap();
            for (Iterator iter = requestParams.keySet().iterator(); iter.hasNext(); ) {
                String name = (String) iter.next();
                String[] values = requestParams.get(name);
                String valueStr = "";
                for (int i = 0; i < values.length; i++) {
                    valueStr = (i == values.length - 1) ? valueStr + values[i] : valueStr + values[i] + ",";
                }
                params.put(name, valueStr);
            }

            boolean verifyResult = AlipaySignature.rsaCheckV1(params, alipayConfig.getAlipayPublicKey(), alipayConfig.getCharset(), alipayConfig.getSignType());
            return verifyResult;
        } catch (AlipayApiException e) {
            log.debug("verify sigin error, exception is:{}", e);
            return false;
        }
    }

    @Override
    public ResultMap refund(String orderNo, double amount, String refundReason) {
        if(StringUtils.isEmpty(orderNo)){
            return ResultMap.error("订单编号不能为空");
        }
        if(amount <= 0){
            return ResultMap.error("退款金额必须大于0");
        }

        AlipayTradeRefundModel model=new AlipayTradeRefundModel();
        // 商户订单号
        model.setOutTradeNo(orderNo);
        // 退款金额
        model.setRefundAmount(String.valueOf(amount));
        // 退款原因
        model.setRefundReason(refundReason);
        // 退款订单号(同一个订单可以分多次部分退款，当分多次时必传)
        // model.setOutRequestNo(UUID.randomUUID().toString());
        AlipayTradeRefundRequest alipayRequest = new AlipayTradeRefundRequest();
        alipayRequest.setBizModel(model);
        AlipayTradeRefundResponse alipayResponse = null;
        try {
            alipayResponse = alipayClient.execute(alipayRequest);
        } catch (AlipayApiException e) {
            log.error("订单退款失败，异常原因:{}", e);
        }
        if(alipayResponse != null){
            String code = alipayResponse.getCode();
            String subCode = alipayResponse.getSubCode();
            String subMsg = alipayResponse.getSubMsg();
            if("10000".equals(code)
                    && StringUtils.isEmpty(subCode)
                    && StringUtils.isEmpty(subMsg)){
                // 表示退款申请接受成功，结果通过退款查询接口查询
                // 修改用户订单状态为退款
                return ResultMap.ok("订单退款成功");
            }
            return ResultMap.error(subCode + ":" + subMsg);
        }
        return ResultMap.error("订单退款失败");
    }

}