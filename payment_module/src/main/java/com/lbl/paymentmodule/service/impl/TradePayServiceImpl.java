package com.lbl.paymentmodule.service.impl;

import com.lbl.paymentmodule.api.pojo.TradeMqProducerTemp;
import com.lbl.paymentmodule.api.pojo.TradePay;
import com.lbl.paymentmodule.api.service.TradePayService;
import com.lbl.paymentmodule.mapper.TradeMqProducerTempMapper;
import com.lbl.paymentmodule.mapper.TradePayMapper;
import org.apache.dubbo.config.annotation.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
/**
 * @author  lbl
 * @date  2020/7/17 14:06
 */
@Service(version = "1.0.0",interfaceClass = TradePayService.class
        ,cluster = "failfast",loadbalance = "roundrobin")
public class TradePayServiceImpl implements TradePayService {

    @Autowired
    private TradePayMapper tradePayMapper;

    @Autowired
    private TradeMqProducerTempMapper tradeMqProducerTempMapper;

    @Override
    public int insertPay(TradePay tradePay) {
        return tradePayMapper.insert(tradePay);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public int updateLocalTransaction(TradePay tradePay, TradeMqProducerTemp tradeMqProducerTemp) {
        int i=0;
        i+=tradePayMapper.insert(tradePay);
        i+=tradeMqProducerTempMapper.insert(tradeMqProducerTemp);
        return i;
    }
}
