package com.lbl.paymentmodule.stream;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

/**
 * @author lbl
 * @version 1.0
 * @date 2020/8/2 9:20
 */
public interface OrderOutPutSources {
    String PAY_ORDER_CALLBACK_OUTPUT="pay-order-callback-output";

    /**
     * 订单回调通道
     * @return
     */
    @Output(PAY_ORDER_CALLBACK_OUTPUT)
    MessageChannel payOrderCallbackOutPut();

}
