package com.lbl.paymentmodule.stream.impl;

import cn.hutool.core.util.StrUtil;
import com.lbl.common.utils.IdWorker;
import com.lbl.ordermodule.api.pojo.TradeOrder;
import com.lbl.ordermodule.api.service.TradeOrderService;
import com.lbl.paymentmodule.api.pojo.TradeMqProducerTemp;
import com.lbl.paymentmodule.api.pojo.TradePay;
import com.lbl.paymentmodule.api.service.TradePayService;
import com.lbl.paymentmodule.mapper.TradePayMapper;
import lombok.extern.slf4j.Slf4j;
import org.apache.rocketmq.spring.annotation.RocketMQTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionListener;
import org.apache.rocketmq.spring.core.RocketMQLocalTransactionState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.Message;
import org.springframework.messaging.MessageHeaders;

import java.math.BigDecimal;
import java.util.Date;
import java.util.Objects;
import java.util.UUID;

/**
 * @author lbl
 * @version 1.0
 * @date 2020/7/30 10:21
 */
@Slf4j
@RocketMQTransactionListener(txProducerGroup = "payOrderCallbackOutputGroup", corePoolSize = 5,
        maximumPoolSize = 10)
public class PayTransactionListenerImpl implements RocketMQLocalTransactionListener {


    @Autowired
    private TradeOrderService tradeOrderService;

    @Autowired
    private TradePayMapper tradePayMapper;

    @Autowired
    private IdWorker idWorker;

    @Autowired
    private TradePayService tradePayService;

    @Override
    public RocketMQLocalTransactionState executeLocalTransaction(Message message, Object o) {
        MessageHeaders headers = message.getHeaders();
        String orderId =headers.get("orderId", String.class);
        try {
            String payId =headers.get("payId", String.class);
            String totalAmount =headers.get("totalAmount", String.class);
            log.info("支付回调执行本地事务begin:{}",orderId);
            if(StrUtil.isNotBlank(orderId)){
                Long orderNo = Long.parseLong(orderId);
                TradePay tradePay=new TradePay();
                tradePay.setPayId(payId);
                tradePay.setOrderId(orderNo);
                tradePay.setPayAmount(BigDecimal.valueOf(Double.parseDouble(totalAmount)));
                tradePay.setIsPaid(2);
                TradeMqProducerTemp tradeMqProducerTemp=new TradeMqProducerTemp();
                tradeMqProducerTemp.setCreateTime(new Date());
                tradeMqProducerTemp.setMsgStatus(0);
                tradeMqProducerTemp.setId(Objects.requireNonNull(headers.get("id", UUID.class)).toString());
                tradeMqProducerTemp.setMsgBody(orderId);
                tradeMqProducerTemp.setMsgKey("*");
                tradeMqProducerTemp.setMsgTag("*");
                tradeMqProducerTemp.setMsgTopic(headers.get("rocketmq_TOPIC", String.class));
                tradeMqProducerTemp.setMsgStatus(headers.get("rocketmq_FLAG", Integer.class));
                //执行本地事务
                tradePayService.updateLocalTransaction(tradePay,tradeMqProducerTemp);
            }
            log.info("支付回调执行本地事务commit:{}",orderId);
            return RocketMQLocalTransactionState.COMMIT;
        } catch (Exception e) {
            e.printStackTrace();
            log.info("支付回调执行本地事务rollback:{}",orderId);
            return RocketMQLocalTransactionState.ROLLBACK;
        }
    }

    @Override
    public RocketMQLocalTransactionState checkLocalTransaction(Message message) {
        MessageHeaders headers = message.getHeaders();
        String orderId =headers.get("orderId", String.class);
        log.info("rocket mq check检测本地事务 [{}]",orderId);
        TradePay tradePay = tradePayMapper.selectByPrimaryKey(orderId);
        log.info("rocket mq check检测本地事务状态 [{}]",Objects.nonNull(tradePay));
        return Objects.nonNull(tradePay)?RocketMQLocalTransactionState.COMMIT:RocketMQLocalTransactionState.ROLLBACK;
    }
}
