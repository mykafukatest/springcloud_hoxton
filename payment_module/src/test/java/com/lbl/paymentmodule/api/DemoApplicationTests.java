package com.lbl.paymentmodule.api;

import com.lbl.paymentmodule.api.pojo.TradeMqConsumerLog;
import com.lbl.paymentmodule.mapper.TradeMqConsumerLogMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

@SpringBootTest
class DemoApplicationTests {

    @Autowired
    private TradeMqConsumerLogMapper mqConsumerLogMapper;

    @Test
    void contextLoads() {
        System.out.println(mqConsumerLogMapper.selectAll());
    }

}
