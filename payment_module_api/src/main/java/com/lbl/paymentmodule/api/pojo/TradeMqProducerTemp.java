package com.lbl.paymentmodule.api.pojo;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import lombok.Data;

@Data
@Table(name = "trade_mq_producer_temp")
public class TradeMqProducerTemp implements Serializable {
    @Id
    @Column(name = "id")
    private String id;

    @Column(name = "group_name")
    private String groupName;

    @Column(name = "msg_topic")
    private String msgTopic;

    @Column(name = "msg_tag")
    private String msgTag;

    @Column(name = "msg_key")
    private String msgKey;

    @Column(name = "msg_body")
    private String msgBody;

    /**
     * 0:未处理;1:已经处理
     */
    @Column(name = "msg_status")
    private Integer msgStatus;

    @Column(name = "create_time")
    private Date createTime;
}