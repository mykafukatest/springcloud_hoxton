package com.lbl.paymentmodule.api.pojo;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.*;
import lombok.Data;

@Data
@Table(name = "trade_pay")
public class TradePay implements Serializable {
    /**
     * 支付编号
     */
    @Id
    @Column(name = "pay_id")
    private String payId;

    /**
     * 订单编号
     */
    @Column(name = "order_id")
    private Long orderId;

    /**
     * 支付金额
     */
    @Column(name = "pay_amount")
    private BigDecimal payAmount;

    /**
     * 是否已支付 1否 2是
     */
    @Column(name = "is_paid")
    private Integer isPaid;
}