package com.lbl.paymentmodule.api.service;

import com.lbl.paymentmodule.api.pojo.TradeMqProducerTemp;
import com.lbl.paymentmodule.api.pojo.TradePay;

/**
 * @author  lbl
 * @date  2020/7/17 14:06
 */
public interface TradePayService{

    int insertPay(TradePay tradePay);

    /**
     * 更新本地事务
     */
    int updateLocalTransaction(TradePay tradePay, TradeMqProducerTemp tradeMqProducerTemp);
}
