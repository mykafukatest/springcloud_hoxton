package com.lbl.search.module;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @author lbl
 */
@SpringBootApplication
@EnableDiscoveryClient
public class SearchModuleApplication {

    public static void main(String[] args) {
        SpringApplication.run(SearchModuleApplication.class, args);
    }

}
