package com.lbl.search.module.controller;

import lombok.extern.slf4j.Slf4j;
import org.frameworkset.elasticsearch.boot.BBossESStarter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lbl
 * @version 1.0
 * @date 2020/8/8 9:46
 */
@Slf4j
@RestController
@RequestMapping("esTest")
public class EsTestController {
    @Autowired
    private BBossESStarter bbossESStarter;

    @RequestMapping("/existIndex/{index}")
    public boolean existIndex(@PathVariable("index")String index){
        //判读索引是否存在，false表示不存在，正常返回true表示存在
        return  bbossESStarter.getRestClient().existIndice(index);
    }
    @RequestMapping("/existType/{index}/{type}")
    public boolean existType(@PathVariable("index")String index,@PathVariable("type")String type){
        //判读type是否存在，false表示不存在，正常返回true表示存在
        return  bbossESStarter.getRestClient().existIndiceType(index,type);
    }

    public static void main(String[] args) {
        System.out.println("\u4f18\u79c0");
    }
}
