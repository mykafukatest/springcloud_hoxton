package com.lbl.search.module.controller;

import cn.hutool.core.collection.CollUtil;
import com.lbl.search.module.entiry.GoodsSearchEntity;
import lombok.extern.slf4j.Slf4j;
import org.frameworkset.elasticsearch.boot.BBossESStarter;
import org.frameworkset.elasticsearch.entity.ESDatas;
import org.frameworkset.elasticsearch.entity.MetaMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author lbl
 * @version 1.0
 * @date 2020/8/11 14:45
 */
@RestController
@Slf4j
@RequestMapping("search")
public class SearchController {
    /**
     * 价格需要划分的区间
     */
    private static final BigDecimal priceInterRegional=BigDecimal.valueOf(6);
    /**
     * 保留千分位
     */
    private static final BigDecimal THOUSANDS=BigDecimal.valueOf(1000);

    @Autowired
    private BBossESStarter bbossESStarter;

    @RequestMapping("/searchKey")
    public Object searchKey(@RequestParam Map<Object, Object> params){
        Map<Object, Object> resultMap = new HashMap<>();
        ESDatas<GoodsSearchEntity > esDatas = bbossESStarter
                .getConfigRestClient("esMapper/GoodsSearchEntityDsl.xml")
                .searchList("goods/_search","searchKeyWord",
                params, GoodsSearchEntity.class);
        if(CollUtil.isNotEmpty(esDatas.getDatas())){
            BigDecimal max_price =  BigDecimal.valueOf(Double.parseDouble(esDatas.getAggregations().get("max_price").get("value").toString()));
            BigDecimal segment = max_price.divide(priceInterRegional,2, BigDecimal.ROUND_HALF_UP).divide(THOUSANDS,2, BigDecimal.ROUND_HALF_UP);
            List<Map<String,Object>> priceRang=new ArrayList<>();
            BigDecimal start=BigDecimal.ZERO;
            for (int i = 0; i <priceInterRegional.intValue(); i++) {
                Map<String,Object> map=new HashMap<>();
                map.put("left",start);
                start=start.add(segment.multiply(THOUSANDS)).divide(BigDecimal.ONE,0,BigDecimal.ROUND_DOWN);
                map.put("right",start);
                priceRang.add(map);
            }
            Map<String,Object> map=new HashMap<>();
            map.put("left",start);
            priceRang.add(map);
            //价格范围
            resultMap.put("priceRang",priceRang);
            //供应商
            resultMap.put("brand_grep",esDatas.getAggregationBuckets("brand_grep"));
        }
        //返回的数据
        resultMap.put("listData",esDatas.getDatas());

        return resultMap;
    }
}
