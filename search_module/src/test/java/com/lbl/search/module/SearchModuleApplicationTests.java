package com.lbl.search.module;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.google.common.collect.Maps;
import com.lbl.search.module.entiry.UserSearchEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.frameworkset.elasticsearch.boot.BBossESStarter;
import org.frameworkset.elasticsearch.entity.ESDatas;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLEncoder;
import java.util.*;

@SpringBootTest
class SearchModuleApplicationTests {
    @Autowired
    private BBossESStarter bbossESStarter;

    /**
     * 添加文档
     */
    @Test
    void addDocument() {
        System.out.println(bbossESStarter.getRestClient().existIndice("lbl"));
        List<UserSearchEntity> entities=new ArrayList<>();
        for (int i = 0; i < 20; i++) {
            UserSearchEntity userSearchEntity=new UserSearchEntity();
            int random = new Random().nextInt(10);
            userSearchEntity.setId(i);
            userSearchEntity.setName("张三"+i);
            userSearchEntity.setAge(random);
            userSearchEntity.setSex(random%2==0?"男":"女");
            entities.add(userSearchEntity);
        }
        String lzm=bbossESStarter.getRestClient().addDocuments("lzm","user",entities);
        System.out.println(lzm);
    }

    /**
     *修改文档
     */
    @Test
    void updateDocument() {
        HashMap<Object, Object> map = Maps.newHashMap();
        map.put("name","李四");
        map.put("age",30);
        bbossESStarter.getRestClient().updateDocument("lzm","user","14",map);

    }

    /**
     *删除文档
     */
    @Test
    void deleteDocument() {
        bbossESStarter.getRestClient().deleteDocument("lzm","user","19");
    }
    /**
     *根据id获取文档
     */
    @Test
    void getDocumentById() {
        UserSearchEntity document = bbossESStarter.getRestClient().getDocument("lzm", "user", "14",UserSearchEntity.class);
        System.out.println(document);
    }


    /**
     *dsl 搜索
     */
    @Test
    void searchDocumentDsl() {
        Map<Object,Object> params=new HashMap<>();
        params.put("name","张三");
        params.put("start",6);
        params.put("end",10);
        ESDatas<UserSearchEntity> datas = bbossESStarter.getConfigRestClient("esMapper/UserSearchEntityDsl.xml")
                .searchList("lzm/_search", "queryTracesByCriteria", params, UserSearchEntity.class);
        for (UserSearchEntity data : datas.getDatas()) {
            System.out.println(data);
        }
    }


    /**
     *sql 搜索
     */
    @Test
    void searchDocumentSql() {

        List<UserSearchEntity> entities = bbossESStarter.getRestClient()
                .sql(UserSearchEntity.class, "{\"query\": \"SELECT * FROM lzm where name like '%张%' and sex='男'\"}");
        for (UserSearchEntity data : entities) {
            System.out.println(data);
        }
    }

    /**
     *sql file 占位符 搜索
     */
    @Test
    void searchDocumentSqlFile() {
        Map<Object,Object> params = new HashMap<>();
        params.put("name","%张三%");
        List<UserSearchEntity> entities = bbossESStarter.getConfigRestClient("esMapper/UserSearchEntitySql.xml")
                .sql(UserSearchEntity.class,"findByName",params);
        for (UserSearchEntity data : entities) {
            System.out.println(data);
        }
    }

    /**
     *爬页面
     */
    @Test
    void crawlPage() throws Exception {
        List<Content> content = getContent("二手书");
        bbossESStarter.getRestClient().addDocuments("goods","good",content);

    }

    private static List<Content> getContent(String keyword) throws Exception {
        keyword= URLEncoder.encode(keyword,"UTF-8");
        String url=String.format("https://search.jd.com/Search?keyword=%s",keyword);
        Document parse = Jsoup.parse(new URL(url), 30000);
        Element goodsList = parse.getElementById("J_goodsList");
        Elements li = goodsList.getElementsByTag("li");
        List<Content> array=new ArrayList<>();
        Random random = new Random();
        for (Element element : li) {
            String imgUrl = element.getElementsByTag("img").eq(0).attr("src");
            String price = element.getElementsByTag("i").eq(0).text();
            String title= element.getElementsByClass("promo-words").eq(0).text();
            String brand= element.getElementsByClass("curr-shop hd-shopname").eq(0).text();
            if(StrUtil.isNotBlank(price)&&StrUtil.isNotBlank(imgUrl)&&StrUtil.isNotBlank(title)){
                array.add(new Content(title,Double.parseDouble(price),imgUrl,brand,random.nextInt(100000)));
            }
        }
        return array;
    }
}
@Data
@AllArgsConstructor
@NoArgsConstructor
class Content{
    private String title;
    private double price;
    private String img;
    private String brand;
    private Integer commit;
}