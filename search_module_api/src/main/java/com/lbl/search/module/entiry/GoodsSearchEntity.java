package com.lbl.search.module.entiry;

import lombok.Data;
import org.frameworkset.elasticsearch.entity.ESBaseData;

/**
 * @author lbl
 * @version 1.0
 * @date 2020/8/11 14:46
 */
@Data
public class GoodsSearchEntity extends ESBaseData {
    private String title;
    private double price;
    private String img;
    private String brand;
    private Integer commit;
}
