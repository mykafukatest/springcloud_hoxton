package com.lbl.search.module.entiry;

import com.frameworkset.orm.annotation.ESId;
import com.frameworkset.orm.annotation.ESMetaFields;
import lombok.Data;
import lombok.ToString;

import java.lang.annotation.Documented;

/**
 * @author lbl
 * @version 1.0
 * @date 2020/8/10 9:35
 */
@Data
@ToString
public class UserSearchEntity {
    @ESId
    private Integer id;
    @ESMetaFields
    private String name;
    private Integer age;
    private String sex;
    private String desc;
}
