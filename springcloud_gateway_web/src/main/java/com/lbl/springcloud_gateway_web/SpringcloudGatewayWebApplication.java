package com.lbl.springcloud_gateway_web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class SpringcloudGatewayWebApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringcloudGatewayWebApplication.class, args);
    }

}
