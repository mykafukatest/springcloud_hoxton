package com.lbl.springcloud_gateway_web.config;

import com.alibaba.cloud.nacos.ribbon.NacosRule;
import com.netflix.loadbalancer.IRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author lbl
 * @version 1.0
 * @date 2020/7/27 10:45
 */
@Configuration
public class GateWayConfig {


//    @Bean
    public IRule loadBalanceRule(){
        return new NacosRule();
    }
}
