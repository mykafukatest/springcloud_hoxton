package com.lbl.common.utils;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class PageResult<T> implements Serializable {
    private static final long serialVersionUID = 1L;
    private long total;
    private List<T> list;

    @Override
    public String toString() {
        return "PageSerializable{total=" + this.total + ", list=" + this.list + '}';
    }
}